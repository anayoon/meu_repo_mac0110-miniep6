# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    for i in 1:n^3
		if i%2!=0
			k=i
			total=0
			soma=0
			while soma<n^3
				soma+=k
				k+=2
				total+=1
			end
			if soma==n^3 && total==n
				return i
			end
		end
	end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    for i in 1:m^3
		if i%2!=0
			k=i
			total=0
			soma=0
			while soma<m^3
				soma+=k
				k+=2
				total+=1
			end
			if soma==m^3 && total==m
				print(m)
				print(" ")
				print(m^3)
				print(" ")
				for j in 1:m
					print(i)
					print(" ")
					i+=2
				end
				println()#para quebrar a linha
			end
		end
	end
end

function mostra_n(n)
	for i in 1:n
		imprime_impares_consecutivos(i)
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
